import pygame 
pygame.init()

Ancho= 700
Alto = 450
Movimiento=150
Tiempo= pygame.time.Clock()


#Colores 
Blanco= (255, 255, 255)
Negro= (0,0,0)
Rojo=(255,0,0)
Azul=(20,117,240)
Vinotinto=(128,0,0)
Amarillo=(255,255,0)
Cafe=(90,50,15)
Verde=(0,255,0)

screen= pygame.display.set_mode((Ancho,Alto))

#fondos 
Escena1=pygame.image.load("IMAGENES/Nivel1.png").convert()
x=0
y=0

#Personaje 
Quieto=  pygame.image.load('IMAGENES/QUIETO.png')

CaminaDerecha= [pygame.image.load('IMAGENES/Derecha1.png'),
                pygame.image.load('IMAGENES/Derecha2.png'),
                pygame.image.load('IMAGENES/Derecha4.png'),
                ]

CaminaIzquierda=[pygame.image.load('IMAGENES/Izquierda1.png'),
                pygame.image.load('IMAGENES/Izquierda2.png'),
                pygame.image.load('IMAGENES/Izquierda3.png'),]

Saltar=[pygame.image.load('IMAGENES/Salto1.png')]

x = 0
px = 50
py = 200
ancho = 40
velocidad = 10

#Variables de movimiento 

salto=False 
cuentaSalto=10

Izquierda=False
Derecha=False 

CuentaPasos=0

def movimiento ():
    global CuentaPasos 
    global x
    
#Bucle para el movimiento de la escena 
    x_Final= x % Escena1.get_rect().width
    screen.blit(Escena1,(x_Final - Escena1.get_rect().width ,0))
    if x_Final < Ancho:
        screen.blit(Escena1,(x_Final, 0))
    x-=1
    
    if CuentaPasos + 1>=3:
        CuentaPasos=0
    
    if Izquierda:
        screen.blit(CaminaDerecha[CuentaPasos//1], (int(px), int(py)))
        CuentaPasos +=1
    
    elif Derecha:
        screen.blit(CaminaDerecha[CuentaPasos // 1], (int(px), int(py)))
        CuentaPasos +=1
        
    elif salto + 1>=2:
        screen.blit(salta[CuentaPasos // 1], (int(px), int(py)))
		CuentaPasos += 1

	else:
		screen.blit(QUIETO,(int(px), int(py)))

ejecuta = True
#-----------------
while ejecuta:
    	# FPS
	reloj.tick(18)

	# Bucle del juego
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			ejecuta = False

	# Opción tecla pulsada
	keys = pygame.key.get_pressed()

	# Tecla A - Moviemiento a la izquierda
	if keys[pygame.K_a] and px > velocidad:
		px -= velocidad
		Izquierda = True
		Derecha = False

	# Tecla D - Moviemiento a la derecha
	elif keys[pygame.K_d] and px < 900 - velocidad - ancho:
		px += velocidad
		Izquierda = False
		Derecha = True

	# Personaje quieto
	else:
		Izquierda = False
		Derecha = False
		CuentaPasos = 0

	# Tecla W - Moviemiento hacia arriba
	if keys[pygame.K_w] and py > 100:
		py -= velocidad

	# Tecla S - Moviemiento hacia abajo
	if keys[pygame.K_s] and py < 300:
		py += velocidad
	# Tecla SPACE - Salto
	if not salto:
		if keys[pygame.K_SPACE]:
			salto = True
			Izquierda = False
			Derecha = False
			CuentaPasos = 0
	else:
		if cuentaSalto >= -10:
			py -= (cuentaSalto * abs(cuentaSalto)) * 0.5
			cuentaSalto -= 1
		else:
			cuentaSalto = 10
			salto = False

	# Actualización de la ventana
	pygame.display.update()
	#Llamada a la función de actualización de la ventana
	recarga_pantalla()

#JUGADORES Y TITULO 
pygame.display.set_caption ("Jugador1")
Jugador1=pygame.image.load("IMAGENES/Jugador1.png")
pygame.display.set_icon(Jugador1)

running=True
while running:
#bucle para cerrar
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.quit()
            


    pygame.display.update()
    
    Tiempo.tick(Movimiento)

 
pygame.quit()